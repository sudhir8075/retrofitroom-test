package com.example.retrofitexample;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.example.adapter.UsersAdpter;
import com.example.model.Example;
import com.example.viewmodel.UsersViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    UsersAdpter adapter;
    List<Example> usersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_list);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        UsersViewModel model = ViewModelProviders.of(this).get(UsersViewModel.class);

        model.getHeroes().observe(this, new Observer<List<Example>>() {
            @Override
            public void onChanged(@Nullable List<Example> heroList) {
                adapter = new UsersAdpter(MainActivity.this, heroList);
                recyclerView.setAdapter(adapter);
            }
        });
    }
}