package com.example.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.model.Example;

import java.util.List;

@Dao
public interface UsersDao {

    @Query("SELECT * FROM example")
    LiveData<List<Example>> getAll();

    @Insert
    void insert(Example task);

    @Delete
    void delete(Example task);

    @Update
    void update(Example task);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertUsers(List<Example> order);

}