package com.example.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.model.Example;

@Database(entities = {Example.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UsersDao taskDao();
}