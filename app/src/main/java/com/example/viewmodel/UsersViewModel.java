package com.example.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.database.DatabaseClient;
import com.example.model.Example;
import com.example.remote.Api;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UsersViewModel extends AndroidViewModel {

    //this is the data that we will fetch asynchronously
    private MutableLiveData<List<Example>> heroList;
    Application application;

    public UsersViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    //we will call this method to get the data
    public LiveData<List<Example>> getHeroes() {
        //if the list is null
        if (heroList == null) {
            heroList = new MutableLiveData<List<Example>>();
            //we will load it asynchronously from server in this method
            loadHeroes();
        }

        //finally we will return the list
        return heroList;
    }


    //This method is using Retrofit to get the JSON data from URL
    private void loadHeroes() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Call<List<Example>> call = api.getUsers();


        call.enqueue(new Callback<List<Example>>() {
            @Override
            public void onResponse(Call<List<Example>> call, Response<List<Example>> response) {

                //finally we are setting the list to our MutableLiveData
                heroList.setValue(response.body());
                DatabaseClient.getInstance(application).getAppDatabase()
                        .taskDao()
                        .insertUsers(response.body());
            }

            @Override
            public void onFailure(Call<List<Example>> call, Throwable t) {

            }
        });
    }
}
