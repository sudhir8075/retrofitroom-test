package com.example.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.model.Example;
import com.example.retrofitexample.R;

import java.util.List;

public class UsersAdpter extends RecyclerView.Adapter<UsersAdpter.UserViewHolder> {

    Context mCtx;
    List<Example> usersList;

    public UsersAdpter(Context mCtx, List<Example> heroList) {
        this.mCtx = mCtx;
        this.usersList = heroList;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.list_item, parent, false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        Example hero = usersList.get(position);


        holder.textViewName.setText(hero.getName());
        holder.textViewNumber.setText(hero.getPhone());
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    class UserViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textViewName,textViewNumber;

        public UserViewHolder(View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.textView_name);
            textViewNumber = itemView.findViewById(R.id.textView_number);
        }
    }
}